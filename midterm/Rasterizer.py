'''
Created on June 12, 2016

@author: Srinivas
'''

class Rasterizer(object):

    def populateEdgeTable(self, x, y):
        """
        Constructs and returns an Edge Table from a list of vertices 
        representing a polygon. The Edge Table contains a list of buckets. 
        A bucket contains the following values:
            [y_max, x, dx, dy, sum]
        
        :param x: The x co-ordinates of the vertices of the polygon
        :param y: The y co-ordinates of the vertices of the polygon

        :return: The edge table
        """
        # Initialize the edge table for scan lines within the polygon's bounds
        y_min = min(y)
        edge_table = [[] for _ in range(y_min, max(y) + 1)]
        n = len(x)
        
        # Create buckets for all edges
        for i in range(n):
            i_plus_1 = 0 if i == (n-1) else (i+1)
            y_min_idx = i if min(y[i], y[i_plus_1]) == y[i] else (i_plus_1)
            y_max_idx = i_plus_1 if y_min_idx == i else i 
            bucket = [y[y_max_idx], x[y_min_idx], x[y_max_idx] - x[y_min_idx],
                      y[y_max_idx] - y[y_min_idx], 0]
            edge_table[y[y_min_idx] - y_min].append(bucket)
        return edge_table

    def updateBuckets(self, active_edge_table):
        """
        Updates the 'x' values in the active edge table for the next
        scan line.

        :param active_edge_table: The Active Edge Table
        """
        for bucket in active_edge_table:
            bucket[4] += bucket[2]
            x_inc = 1 if bucket[2] > 0 else -1
            while abs(bucket[4]) >= bucket[3]:
                bucket[4] = (bucket[4] + bucket[3]) if x_inc == -1 \
                            else (bucket[4] - bucket[3])
                bucket[1] += x_inc

    def drawPolygon(self, x, y, cgc):
        """
        Draws a filled polygon with the given vertices on the cgCanvas cgc.

        :param x: The x co-ordinates of the vertices of the polygon
        :param y: The y co-ordinates of the vertices of the polygon
        :param cgc: A cgCanvas object
        """
        y_min, y_max = min(y), max(y)
        edge_table = self.populateEdgeTable(x, y)
        active_edge_table = []

        for y in range(y_min, y_max + 1):
            # Add any edge table buckets at the current scan line to the
            # active edge table.
            for bucket in edge_table[y - y_min]:
                active_edge_table.append(bucket)

            # Remove edges ending at the current scan line from the active
            # edge table.
            active_edge_table = [bucket for bucket in active_edge_table \
                                 if bucket[0] != y]

            # Sort the active edge table on x
            active_edge_table.sort(key=lambda bucket: bucket[1])

            # Set pixels between pairs of active edges
            for i in range(len(active_edge_table[::2])):
                for x in range(active_edge_table[2*i][1], \
                               active_edge_table[2*i+1][1]):
                    cgc.setPixel(x, y)

            # Update the buckets in the active edge table for the next scan
            # line
            self.updateBuckets(active_edge_table)
