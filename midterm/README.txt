CSCI 610: Foundations of Computer Graphics
Midterm Project
Submitted by: Nalin Ranjan

Files submitted:
    - cgCanvas.py
    - Rasterizer.py
    - Clipper.py
    - README.txt

Operating Sytem: Microsoft Windows 10 (64-bit)
Language: Python 3.6.2 (64-bit)

Other info:
The submitted files need to be in the same directory when run.
NumPy used for matrix multiplication.