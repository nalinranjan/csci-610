import math
import numpy as np
from simpleCanvas import simpleCanvas
from Clipper import Clipper
from Rasterizer import Rasterizer

class cgCanvas(simpleCanvas):

    def __init__(self, w, h):
        """
        Initializes the cgCanvas.

        :param w: The width of the canvas
        :param h: The height of the canvas
        """
        self.setSize(w, h)
        self.setClipWindow(0, h, 0, w)
        self.setViewport(0, 0, w, h)
        self.clearTransform()

        self.polygons = []
        self.clipper = Clipper()
        self.rasterizer = Rasterizer()

    def addPoly(self, x, y, n):
        """
        Adds and stores a polygon to the canvas. Note that this method does not
        draw the polygon, but merely stores it for later draw. Drawing is
        initiated by the draw() method.

        Returns a unique integer id for the polygon.

        :param x: Array of x coords of the vertices of the polygon to be added.
        :param y: Array of y coords of the vertices of the polygin to be added.
        :param n: Number of verticies in polygon

        :return: A unique integer identifier for the polygon
        """
        self.polygons.append(list(zip(x[:n], y[:n])))
        return len(self.polygons) - 1

    def clearTransform(self):
        """
        Sets the current transformation to be the identity
        """
        self.transform = np.identity(3)

    def drawPoly(self, polyID):
        """
        Draws the polygon with the given id. Draw should draw the polygon after
        applying the current transformation on the vertices of the polygon.

        :param polyID: The ID of the polygon to be drawn.
        """
        # Retreive the required polygon
        polygon = self.polygons[polyID]
        transformed_poly = []

        # Apply the current transform to all vertices
        for x, y in polygon:
            vertex = np.array([x, y, 1]).reshape(3, 1)
            vertex = self.transform @ vertex
            transformed_poly.append((vertex[0][0], vertex[1][0]))

        # Clip the polygon
        transformed_poly = self.clipper.clipPolygon(transformed_poly, \
                                                    *self.clipWindow)

        # Transform the polygon's vertices to screen co-ordinates
        transformed_poly = self.viewTransform(transformed_poly)

        # Draw the polygon
        self.rasterizer.drawPolygon(*zip(*transformed_poly), self)

    def rotate(self, degrees):
        """
        Adds a rotation to the current transformation by premultiplying the
        appropriate rotation matrix to the current transformtion matrix.

        :param degrees: Amount of rotation in degrees.
        """
        matrix = np.identity(3)
        matrix[0][0] = matrix[1][1] = math.cos(math.radians(degrees))
        matrix[1][0] = matrix[0][1] = math.sin(math.radians(degrees))
        matrix[0][1] *= -1
        self.transform = matrix @ self.transform

    def scale(self, x, y):
        """
        Adds a scale to the current transformation by premultiplying the
        appropriate scaling matrix to the current transformtion matrix.

        :param x: Amount of scaling in x.
        :param y: Amount of scaling in y.
        """
        matrix = np.identity(3)
        matrix[0][0] = x
        matrix[1][1] = y
        self.transform = matrix @ self.transform

    def translate(self, x, y):
        """
        Adds a translation to the current transformation by premultiplying
        the appropriate translation matrix to the current transformtion matrix.

        :param x: Amount of translation in x.
        :param y: Amount of translation in y.
        """
        matrix = np.identity(3)
        matrix[0][2] = x
        matrix[1][2] = y
        self.transform = matrix @ self.transform

    def setClipWindow(self, bottom, top, left, right):
        """
        Defines the clip window.

        :param bottom: y coord of bottom edge of clip window (in world coords)
        :param top: y coord of top edge of clip window (in world coords)
        :param left: x coord of left edge of clip window (in world coords)
        :param right: x coord of right edge of clip window (in world coords)
        """
        self.clipWindow = (bottom, top, left, right)

    def setViewport(self, x, y, width, height):
        """
        Defines the viewport.

        :param xmin: x coord of lower left of view window (in screen coords)
        :param ymin: y coord of lower left of view window (in screen coords)
        :param width: width of view window (in screen coords)
        :param height: width of view window (in screen coords)
        """
        self.viewport = (int(x), int(y), int(width), int(height))

    def viewTransform(self, vertices):
        """
        Transforms the given vertices of a polygon from world co-ordinates to
        screen co-ordinates.

        :param vertices: The vertices of the polygon in world co-ordinates

        :return: The vertices of the polygon in screen co-ordinates
        """
        # Save the current transform
        current_transform = self.transform
        self.clearTransform()
        out_vertices = []

        # Translate the clip window to the origin
        self.translate(-self.clipWindow[2], -self.clipWindow[0])

        # Scale to viewport
        self.scale(self.viewport[2]/(self.clipWindow[3] - self.clipWindow[2]),
                   self.viewport[3]/(self.clipWindow[1] - self.clipWindow[0]))

        # Translate to position of view window
        self.translate(self.viewport[0], self.viewport[1])

        # Apply the composite transform to all vertices
        for x, y in vertices:
            vertex = np.array([x, y, 1]).reshape(3, 1)
            vertex = self.transform @ vertex
            out_vertices.append((int(vertex[0][0]), int(vertex[1][0])))

        # Reset the current transform to the saved value
        self.transform = current_transform
        return out_vertices
