from OpenGL.GL import *
import numpy as np
import math

class viewParams(object):

    r, l = -1.0, 1.0
    b, t = -1.0, 1.0
    n, f = 0.9, 4.5

    def setUpFrustrum(self, program):
        '''
        Sets up the projection parameter for a frustum projection of the scene
        and sends it to the vertex shader. The values used for projection are:
            Bottom: -1.0    Top  : 1.0
            Left  : -1.0    Right: 1.0
            Near  : 0.9     Far  : 4.5

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        '''
        projection = np.array([[2*self.n/(self.r - self.l), 0.0, (self.r + self.l)/(self.r - self.l), 0.0],
                               [0.0, 2*self.n/(self.t - self.b), (self.t + self.b)/(self.t - self.b), 0.0],
                               [0.0, 0.0, -(self.f + self.n)/(self.f - self.n), -2*self.f*self.n/(self.f - self.n)],
                               [0.0, 0.0, -1.0, 0.0]])
        projectionLocation = glGetUniformLocation(program, "projection")
        self.setUniform(projectionLocation, projection)

    def setUpOrtho(self, program):
        '''
        Sets up the projection parameter for an orthographic projection of the
        scene and sends it to the vertex shader. The values used for projection
        are:
            Bottom: -1.0    Top  : 1.0
            Left  : -1.0    Right: 1.0
            Near  : 0.9     Far  : 4.5

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        '''
        projection = np.array([[2/(self.r - self.l), 0.0, 0.0, -(self.r + self.l)/(self.r - self.l)],
                               [0.0, 2/(self.t - self.b), 0.0, -(self.t + self.b)/(self.t - self.b)],
                               [0.0, 0.0, -2/(self.f - self.n), -(self.f + self.n)/(self.f - self.n)],
                               [0.0, 0.0, 0.0, 1.0]])
        projectionLocation = glGetUniformLocation(program, "projection")
        self.setUniform(projectionLocation, projection)

    def clearTransform(self, program):
        '''
        Clears any transformations, setting the values to the defaults: no
        scaling (scale factor of 1), no rotation (degree of rotation = 0), and
        no translation (0 translation in each direction), and sends the
        parameters to the vertex shader.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        '''
        transform = np.identity(4)
        transformLocation = glGetUniformLocation(program, "model")
        self.setUniform(transformLocation, transform)

    def setUpTransform(self, program, xScale, yScale, zScale,
                       xRotate, yRotate, zRotate,
                       xTranslate, yTranslate, zTranslate):
        '''
        Sets up the transformation parameters for the vertices of the teapot
        and sends the parameters to the vertex shader. The order of application
        is specified in the driver program.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        :param xScale: Amount of scaling along the x-axis
        :param yScale: Amount of scaling along the y-axis
        :param zScale: Amount of scaling along the z-axis
        :param xRotate: Angle of rotation around the x-axis, in degrees
        :param yRotate: Angle of rotation around the y-axis, in degrees
        :param zRotate: Angle of rotation around the z-axis, in degrees
        :param xTranslate: Amount of translation along the x axis
        :param yTranslate: Amount of translation along the y axis
        :param zTranslate: Amount of translation along the z axis
        '''
        transform = np.identity(4)

        # Apply scaling
        for i, scale in enumerate([xScale, yScale, zScale]):
            transform[i][i] = scale

        # Apply rotation along the z axis
        rotateZ = np.array([[math.cos(math.radians(zRotate)), math.sin(math.radians(zRotate))],
                            [-math.sin(math.radians(zRotate)), math.cos(math.radians(zRotate))]])
        rotate = np.identity(4)
        rotate[0:2, 0:2] = rotateZ
        transform = rotate @ transform

        # Apply rotation along the y axis
        rotateY = np.array([[math.cos(math.radians(yRotate)), math.sin(math.radians(yRotate))],
                            [-math.sin(math.radians(yRotate)), math.cos(math.radians(yRotate))]])
        rotate = np.identity(4)
        rotate[0:3:2, 0:3:2] = rotateY.transpose()
        transform = rotate @ transform

        # Apply translation
        translate = np.identity(4)
        translate[3, 0:3] = np.array([xTranslate, yTranslate, zTranslate])
        transform = translate @ transform

        transformLocation = glGetUniformLocation(program, "model")
        self.setUniform(transformLocation, transform)
    
    def clearCamera(self, program):
        '''
        Clears any changes made to camera parameters, setting the values to the
        defaults: eyepoint (0.0,0.0,0.0), lookat (0,0,0.0,-1.0), and up vector
        (0.0,1.0,0.0), and sends the parameters to the vertex shader.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        '''
        self.setUpCamera(program, *[0.0]*5, -1.0, 0.0, 1.0, 0.0)

    def setUpCamera(self, program, eyePointX, eyePointY, eyePointZ,
                    lookatX, lookatY, lookatZ,
                    upX, upY, upZ):
        '''
        Sets up the camera parameters controlling the viewing transformation
        and sends the parameters to the vertex shader.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        :param eyepointX: x coordinate of the camera location
        :param eyepointY: y coordinate of the camera location
        :param eyepointZ: z coordinate of the camera location
        :param lookatX: x coordinate of the lookat point
        :param lookatY: y coordinate of the lookat point
        :param lookatZ: z coordinate of the lookat point
        :param upX: x coordinate of the up vector
        :param upY: y coordinate of the up vector
        :param upZ: z coordinate of the up vector
        '''
        eyePoint = np.array([eyePointX, eyePointY, eyePointZ])
        lookat = np.array([lookatX, lookatY, lookatZ])
        up = np.array([upX, upY, upZ])

        # Set up the n, u and v vectors for the camera
        n = self.normalize(eyePoint - lookat)
        u = self.normalize(np.cross(up, n))
        v = self.normalize(np.cross(n, u))

        camera = np.array([[u[0], u[1], u[2], -1*np.dot(u, eyePoint)],
                           [v[0], v[1], v[2], -1*np.dot(v, eyePoint)],
                           [n[0], n[1], n[2], -1*np.dot(n, eyePoint)],
                           [0.0, 0.0, 0.0, 1.0]])
        viewLocation = glGetUniformLocation(program, "view")
        self.setUniform(viewLocation, camera)

    def setUniform(self, uniformLocation, matrix):
        '''
        Sends a 4x4 matrix to a given uniform variable in the vertex shader.

        :param uniformLocation: The ID of the uniform variable to be set
        :param matrix: The 4x4 matrix to be sent to the vertex shader
        '''
        matrix = matrix.reshape(1, 16)
        glUniformMatrix4fv(uniformLocation, 1, GL_TRUE, matrix)
    
    def normalize(self, vector):
        '''
        Returns a given vector in normalized form.

        :param vector: The vector to be normalized 
        '''
        return vector / np.linalg.norm(vector)