# Clipper.py
#
# Created on June 12, 2016
#
# @author: Srinivas
#
# Contributor:  Nalin Ranjan
#
# Object for performing clipping

from collections import namedtuple

class clipper(object):

    Point = namedtuple('Point', 'x y')
    UPPER = 0
    RIGHT = 1
    LOWER = 2
    LEFT = 3

    def isInside(self, vertex, limit, edge):
        """
        Calculates whether the point 'vertex' lies inside a given edge of
        the clip rectangle.

        :param vertex: The polygon vertex to be be checked
        :param limit: The constant co-ordinate of the clip edge
        :param edge: The clip edge to be considered

        :return: True if the vertex is inside the clip edge, false otherwise
        """
        if edge == self.UPPER:
            return vertex.y <= limit
        elif edge == self.RIGHT:
            return vertex.x <= limit
        elif edge == self.LOWER:
            return vertex.y >= limit
        else:
            return vertex.x >= limit

    def intersection(self, S, P, limit, edge):
        """
        Calculates and returns the intersection point of a polygon edge with
        a given clip edge.

        :param S: The first point of the polygon edge
        :param P: The second point of the polygon edge
        :param limit: The constant co-ordinate of the clip edge
        :param edge: The clip edge to be considered

        :return: The point of intersection
        """
        dy = P.y - S.y
        dx = P.x - S.x
        if dx == 0:
            return self.Point(S.x, limit)
        if dy == 0:
            return self.Point(limit, S.y)
        slope = dy/dx

        if edge == self.UPPER or edge == self.LOWER:
            x = (limit - S.y)/slope + S.x
            return self.Point(int(x), limit)
        else:
            y = (limit - S.x)*slope + S.y
            return self.Point(limit, int(y))


    def clipPolygon(self, in1, inx, iny, outx, outy, llx, lly, urx, ury):
        """
        Clips the polygon with vertex count in1 and vertices inx/iny
        against the rectangular clipping region specified by lower-left corner
        (llx,lly) and upper-right corner (urx,ury). The resulting vertices are
        placed in outx/outy.
        
        Returns the vertex count of the polygon resulting from the clipping.
        
        :param in1: the number of vertices in the polygon to be clipped
        :param inx: x coords of vertices of polygon to be clipped
        :param iny: y coords of vertices of polygon to be clipped
        :param outx: x coords of vertices of polygon resulting after clipping
        :param outy: y coords of vertices of polygon resulting after clipping
        :param llx: x coord of lower left of clipping rectangle
        :param lly: y coord of lower left of clipping rectangle
        :param urx: x coord of upper right of clipping rectangle
        :param ury: y coord of upper right of clipping rectangle

        :return: number of vertices in the polygon resulting after clipping
        """
        vertices = [self.Point(x, y) for x,y in list(zip(inx, iny))]
        lower_left = self.Point(llx, lly)
        upper_right = self.Point(urx, ury)
        edge_coordinates = [upper_right.y, upper_right.x, lower_left.y, lower_left.x]

        # Run the algorithm for all 4 edges of the clip rectangle
        for edge in range(4):
            out_vertices = []
            S = vertices[-1]
            for P in vertices:
                # If P is inside
                if self.isInside(P, edge_coordinates[edge], edge):
                    # If S is outide
                    if not self.isInside(S, edge_coordinates[edge], edge):
                        out_vertices.append(self.intersection(S, P, edge_coordinates[edge], edge))
                    out_vertices.append(P)

                # If P is outside
                else:
                    # If S is inside
                    if self.isInside(S, edge_coordinates[edge], edge):
                        out_vertices.append(self.intersection(S, P, edge_coordinates[edge], edge))
                S = P

            # If there are no output vertices, return 0
            if not out_vertices:
                return 0
            vertices = out_vertices

        outx[:], outy[:] = zip(*[(p.x, p.y) for p in out_vertices])
        return len(outx)
