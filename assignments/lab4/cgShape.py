"""
Created on Aug 4, 2016

@author: Srinivas
"""
from math import cos, sin, radians
from numpy import arange,float32
from simpleShape import simpleShape
import math

class cgShape(simpleShape):
    """
    classdocs
    """


    def __init__(self, params):
        """
        Constructor
        """

    def makeCube(self, subdivisions):
        """
        Creates a unit cube, centered at the origin, with a given number of
        subdivisions in each direction on each face.
        Only uses calls to addTriangle().
        
        :param subdivisions: Number of equal subdivisions to be made in each
                             direction along each face
        """
        if subdivisions < 1:
            subdivisions = 1

        start = -0.5
        end = 0.5
        step = (end - start)/subdivisions

        for axis in range(3):
            for i in arange(start, end, step):
                for j in arange(start, end, step):
                    # Generate vertices for one face parallel to a coordinate plane
                    vertices = [[i, j], [i+step, j], [i+step, j+step],
                                [i+step, j+step], [i, j+step], [i, j]]
                    [vertex.insert(axis, start if axis == 1 else end) for vertex in vertices]
                    self.addTriangle(*[c for vertex in vertices[:3] for c in vertex])
                    self.addTriangle(*[c for vertex in vertices[3:] for c in vertex])

                    # Generate vertices for the other face parallel to the coordinate plane
                    vertices = [[i, j], [i, j+step], [i+step, j+step],
                                [i+step, j+step], [i+step, j], [i, j]]
                    [vertex.insert(axis, end if axis == 1 else start) for vertex in vertices]
                    self.addTriangle(*[c for vertex in vertices[:3] for c in vertex])
                    self.addTriangle(*[c for vertex in vertices[3:] for c in vertex])

    def makeCylinder(self, radius, radialdivision, heightdivision):
        """
        Creates polygons for a cylinder with unit height, centered at the
        origin, with separate number of radial subdivisions and height
        subdivisions.
        Only uses calls to addTriangle().

        :param radius: Radius of the base of the cylinder
        :param radialdivision: Number of subdivisions on the radial base
        :param heightdivision: Number of subdivisions along the height
        """
        if radialdivision < 3:
            radialdivision = 3

        if heightdivision < 1:
            heightdivision = 1

        angle = 360 / radialdivision

        for i in arange(0, 360, angle):
            # Generate vertices for circular disks
            p1 = [radius * math.cos(math.radians(i)), 0.5,
                  radius * math.sin(math.radians(i))]
            p2 = [radius * math.cos(math.radians(i+angle)), 0.5,
                  radius * math.sin(math.radians(i+angle))]
            p3 = [0, 0.5, 0]
            self.addTriangle(*p2, *p1, *p3)
            p1[1] *= -1
            p2[1] *= -1
            p3[1] *= -1
            self.addTriangle(*p1, *p2, *p3)

            # Generate vertices on curvilinear surface
            step = 1 / heightdivision
            for t in arange(-0.5, 0.5, step):
                p1[1] = p2[1] = t
                self.addTriangle(*p2, *p1, p1[0], t+step, p1[2])
                self.addTriangle(p1[0], t+step, p1[2], p2[0], t+step, p2[2], *p2)

    def makeCone(self, radius, radialdivision, heightdivision):
        """
        Creates polygons for a cone with unit height, centered at the origin,
        with separate number of radial subdivisions and height subdivisions.
        Only uses calls to addTriangle().

        :param radius: Radius of the base of the cone
        :param radialdivision: Number of subdivisions on the radial base
        :param heightdivision: Number of subdivisions along the height
        """
        if radialdivision < 3:
            radialdivision = 3

        if heightdivision < 1:
            heightdivision = 1

        angle = 360 / radialdivision

        for i in arange(0, 360, angle):
            # Generate vertices for circular disk
            p1 = [radius * math.cos(math.radians(i)), -0.5,
                  radius * math.sin(math.radians(i))]
            p2 = [radius * math.cos(math.radians(i+angle)), -0.5,
                  radius * math.sin(math.radians(i+angle))]
            p3 = [0, -0.5, 0]
            self.addTriangle(*p1, *p2, *p3)

            # Generate vertices on curved surface
            step = 1 / heightdivision
            _p3, _p4 = p1, p2
            for t in arange(-0.5, 0.5, step):
                _p1, _p2 = _p3, _p4
                _p3 = [(0.5 - t - step) * p1[0], t + step, (0.5 - t - step) * p1[2]]
                _p4 = [(0.5 - t - step) * p2[0], t + step, (0.5 - t - step) * p2[2]]
                self.addTriangle(*_p2, *_p1, *_p3)
                self.addTriangle(*_p3, *_p4, *_p2)

    def makeSphere(self, radius, slices, stacks):
        """
        Creates a sphere of a given radius, centered at the origin, using
        spherical coordinates with separate number of thetha and phi
        subdivisions.
        Only uses calls to addTriangle().

        :param radius: Radius of the sphere
        :param slices: Number of subdivisions in the theta direction
        :param stacks: Number of subdivisions in the phi direction
        """
        if slices < 3:
            slices = 3

        if stacks < 3:
            stacks = 3

        theta_step = 360 / slices
        phi_step = 180 / stacks

        for theta in arange(0, 360, theta_step):
            for phi in arange(0, 180, phi_step):
                # Generate vertices using parametric equation of sphere
                p1 = [math.sin(math.radians(theta)) * math.sin(math.radians(phi)),
                      math.cos(math.radians(phi)),
                      math.cos(math.radians(theta)) * math.sin(math.radians(phi))]
                p1 = [radius * c for c in p1]

                p2 = [math.sin(math.radians(theta)) * math.sin(math.radians(phi+phi_step)),
                      math.cos(math.radians(phi+phi_step)),
                      math.cos(math.radians(theta)) * math.sin(math.radians(phi+phi_step))]
                p2 = [radius * c for c in p2]

                p3 = [math.sin(math.radians(theta+theta_step)) * math.sin(math.radians(phi+phi_step)),
                      math.cos(math.radians(phi+phi_step)),
                      math.cos(math.radians(theta+theta_step)) * math.sin(math.radians(phi+phi_step))]
                p3 = [radius * c for c in p3]

                p4 = [math.sin(math.radians(theta+theta_step)) * math.sin(math.radians(phi)),
                      math.cos(math.radians(phi)),
                      math.cos(math.radians(theta+theta_step)) * math.sin(math.radians(phi))]
                p4 = [radius * c for c in p4]

                self.addTriangle(*p1, *p2, *p4)
                self.addTriangle(*p2, *p3, *p4)
