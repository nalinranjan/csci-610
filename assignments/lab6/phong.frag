#version 150

// Phong fragment shader
//
// Contributor:  Nalin Ranjan

// Vertex position (in camera space)
in vec3 vPositionCam;

// Surface Normal (in camera space)
in vec3 normalCam;

// Light position (in camera space)
in vec3 lightPositionCam;

// Light color
uniform vec4 I_a;
uniform vec4 I_ds;

// Reflection coefficients
uniform float k_a;
uniform float k_d;
uniform float k_s;

// Material color
uniform vec4 O_a;
uniform vec4 O_d;
uniform vec4 O_s;

// Specular exponent
uniform float n;

// Fragment color
out vec4 finalColor;

void main()
{
    // Calculate the required N, L, R and V vectors
    vec3 N = normalize(normalCam);
    vec3 L = normalize(lightPositionCam - vPositionCam);
    vec3 R = normalize(reflect(-L, N));
    vec3 V = normalize(-vPositionCam);

    // Calculate ambient, diffuse and specular components
    vec4 ambient = I_a * k_a * O_a;
    vec4 diffuse = I_ds * k_d * O_d * max(dot(L, N), 0.0);
    vec4 specular = I_ds * k_s * O_s * pow(max(dot(R, V), 0.0), n);

    finalColor = ambient + diffuse + specular;
}
