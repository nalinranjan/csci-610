#version 150

// Texture mapping vertex shader
//
// Contributor:  Nalin Ranjan

// Texture Coordinates
in vec2 texCoord;

// Surface Normal (in camera space)
in vec3 normalCam;

// Textures for smiley and frowny faces
uniform sampler2D smileyTexture;
uniform sampler2D frownyTexture;

// Fragment color
out vec4 finalColor;

void main()
{
    // Display smiley texture on the front face
    if (normalCam.z > 0.0) {
        finalColor = texture(smileyTexture, texCoord);
    }

    // Display frowny texture on the back face
    else {
        finalColor = texture(frownyTexture, texCoord);        
    }
}
