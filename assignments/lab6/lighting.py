from OpenGL.GL import *
from numpy import *

class lighting (object):

    lightPos = [0.0, 5.0, 2.0]
    lightColor = [1.0, 1.0, 0.0, 1.0]
    ambientColor = [0.5, 0.5, 0.5, 1.0]

    oAmbient = [0.5, 0.1, 0.9, 1.0]
    oDiffuse = [0.89, 0.0, 0.0, 1.0]
    oSpecular = [1.0, 1.0, 1.0, 1.0]

    ka, kd, ks = 0.5, 0.7, 1.0
    n = 10.0

    def setUpPhong( self,  program ):
        """
        Sets up the lighting, material, and shading parameters for the Phong
        shader.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        """

        # Send parameter to the vertex shader
        lightPosition = glGetUniformLocation(program, "lightPosition")
        glUniform3fv(lightPosition, 1, self.lightPos)

        # Send parameters to the fragment shader
        ambientColorLocation = glGetUniformLocation(program, "I_a")
        lightColorLocation = glGetUniformLocation(program, "I_ds")
        oAmbientLocation = glGetUniformLocation(program, "O_a")
        oDiffuseLocation = glGetUniformLocation(program, "O_d")
        oSpecularLocation = glGetUniformLocation(program, "O_s")
        kAmbientLocation = glGetUniformLocation(program, "k_a")
        kDiffuseLocation = glGetUniformLocation(program, "k_d")
        kSpecularLocation = glGetUniformLocation(program, "k_s")
        specularExpLocation = glGetUniformLocation(program, "n")

        glUniform4fv(ambientColorLocation, 1, self.ambientColor)
        glUniform4fv(lightColorLocation, 1, self.lightColor)
        glUniform4fv(oAmbientLocation, 1, self.oAmbient)
        glUniform4fv(oDiffuseLocation, 1, self.oDiffuse)
        glUniform4fv(oSpecularLocation, 1, self.oSpecular)
        glUniform1f(kAmbientLocation, self.ka)
        glUniform1f(kDiffuseLocation, self.kd)
        glUniform1f(kSpecularLocation, self.ks)
        glUniform1f(specularExpLocation, self.n)