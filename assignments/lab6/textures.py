from OpenGL.GL import *
import sys, ctypes, platform
from numpy import *
from pysoil import *

class textures (object):

    smileyImagePath = b"smiley2.png"
    frownyImagePath = b"frowny2.png"

    smileyTex = GLuint(0)
    frownyTex = GLuint(0)

    def loadTexture(self):
        """
        Loads texture data for the GPU using SOIL.
        """
        try:
            self.smileyTex = SOIL_load_OGL_texture(
                self.smileyImagePath,
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA
            )
        except Exception as e:
            print('SOIL_load_OGL_texture ERROR', e)

        try:
            self.frownyTex = SOIL_load_OGL_texture(
                self.frownyImagePath,
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y | SOIL_FLAG_MULTIPLY_ALPHA
            )
        except Exception as e:
            print('SOIL_load_OGL_texture ERROR', e)

    def setUpTexture(self, program):
        """
        Sets up the parameters for texture use.

        :param program: The ID of an OpenGL (GLSL) shader program to which
                        parameter values are to be sent
        """
        # Set up the smiley face texture
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.smileyTex)
        smileyLocation = glGetUniformLocation(program, "smileyTexture")
        glUniform1i(smileyLocation, 0)

        # Set up the frowny face texture
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, self.frownyTex)
        frownyLocation = glGetUniformLocation(program, "frownyTexture")
        glUniform1i(frownyLocation, 1)
