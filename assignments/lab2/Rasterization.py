'''
Created on June 12, 2016

@author: Srinivas
'''

class Rasterization(object):

    scan_line = 0

    def __init__(self,n):
        self.scan_line = n


    def populateEdgeTable(self, n, x, y):
        """
        Construct and return an Edge Table from a list of vertices 
        representing a polygon with n sides. The Edge Table contains
        a list of buckets. A bucket contains the following values:
            [y_max, x, dx, dy, sum]
        """
        # Initialize the edge table for scan lines within the polygon's bounds
        y_min = min(y[:n])
        edge_table = [[] for _ in range(y_min, max(y[:n]) + 1)]
        
        # Create buckets for all edges
        for i in range(n):
            i_plus_1 = 0 if i == (n-1) else (i+1)
            y_min_idx = i if min(y[i], y[i_plus_1]) == y[i] else (i_plus_1)
            y_max_idx = i_plus_1 if y_min_idx == i else i 
            bucket = [y[y_max_idx], x[y_min_idx], x[y_max_idx] - x[y_min_idx], \
                      y[y_max_idx] - y[y_min_idx], 0]
            edge_table[y[y_min_idx] - y_min].append(bucket)
        return edge_table

    def updateBuckets(self, active_edge_table):
        """
        Update the 'x' values in the active edge table for the next
        scan line.
        """
        for bucket in active_edge_table:
            bucket[4] += bucket[2]
            x_inc = 1 if bucket[2] > 0 else -1
            while abs(bucket[4]) >= bucket[3]:
                bucket[4] = (bucket[4] + bucket[3]) if x_inc == -1 \
                            else (bucket[4] - bucket[3])
                bucket[1] += x_inc

    def drawPolygon(self, n, x, y, sc):
        """
        Draw a filled polygon in the SimpleCanvas sc.

        The polygon has n distinct vertices. The coordinates of the vertices
        making up the polygon are stored in the x and y lists. The ith
        vertex will have coordinate (x[i], y[i]).
        """
        y_min, y_max = min(y[:n]), max(y[:n])
        edge_table = self.populateEdgeTable(n, x, y)
        active_edge_table = []

        for y in range(y_min, y_max + 1):
            # Add any edge table buckets at the current scan line to the
            # active edge table.
            for bucket in edge_table[y - y_min]:
                active_edge_table.append(bucket)

            # Remove edges ending at the current scan line from the active
            # edge table.
            active_edge_table = [bucket for bucket in active_edge_table if bucket[0] != y]

            # Sort the active edge table on x
            active_edge_table.sort(key=lambda bucket: bucket[1])

            # Set pixels between pairs of active edges
            for i, bucket in enumerate(active_edge_table[::2]):
                for x in range(bucket[1], active_edge_table[i+1][1]):
                    sc.setPixel(x, y)

            # Update the buckets in the active edge table for the next scan
            # line
            self.updateBuckets(active_edge_table)
