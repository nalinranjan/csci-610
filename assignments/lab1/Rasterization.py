'''
Created on June 12, 2016

@author: Srinivas
'''

from SimpleCanvas import SimpleCanvas

class Rasterization(object):

    scan_line = 0

    def __init__(self,n):
        self.scan_line = n


    def drawLine(self, x0, y0, x1, y1, sc):
        """
        Draw a line between the points (x0, y0) and (x1, y1)
        on the canvas represented by sc.
        """

        # Handle vertical lines as a special case.
        if x0 == x1:
            [sc.setPixel(x0, y) for y in range(min(y0, y1), max(y0, y1) + 1)]
            return

        # Handle horizontal lines as a special case.
        if y0 == y1:
            [sc.setPixel(x, y0) for x in range(min(x0, x1), max(x0, x1) + 1)]
            return

        # Swap points if (x1, y1) lies to the left of (x0, y0).
        if x0 > x1:
            x0, x1 = x1, x0
            y0, y1 = y1, y0
        
        dx, dy = x1 - x0, y1 - y0

        # Handle diagonal lines as a special case.
        if dy == dx or dy == -dx:
            y_inc = 1 if dy > 0 else -1
            y0 -= y_inc
            for i in range(dx + 1):
                sc.setPixel(x0 + i, y0 + y_inc)
                y0 += y_inc
            return

        a, b = dy, -dx
        abs_dy = abs(dy)

        x_inc1 = x_inc2 = y_inc1 = y_inc2 = 0

        # Adjust the values of variables based on the slope of the line
        if abs_dy < dx:
            x_inc1 = x_inc2 = 1
            if dy > 0:
                y_inc2 = 1
            else:
                y_inc1 = -1
                b = -b
        else:
            a, b = b, a
            if dy > 0:
                y_inc1 = y_inc2 = 1
                x_inc1 = 1 
            else:
                y_inc1 = y_inc2 = -1
                x_inc2 = 1
                a = -a

        # Set the initial value of the decision variable
        d = a + a + b
        d_inc1 = a + a
        d_inc2 = a + a + b + b

        if dy > dx or (dy < 0 and abs_dy < dx):
            d_inc1, d_inc2 = d_inc2, d_inc1

        x, y = x0, y0

        # Calculate and set the pixels that represent the line
        for _ in range(max(abs_dy, dx)):
            sc.setPixel(x, y)
            if d <= 0:
                d += d_inc1
                x += x_inc1
                y += y_inc1
            else:
                d += d_inc2
                x += x_inc2
                y += y_inc2

    def myInitials(self, sc):
        """
        Draw the initials NR (for Nalin Ranjan) using line segments
        """

        # Use light blue for initials
        sc.setColor (80, 180, 250)

        # Draw the letter 'N'
        self.drawLine(40, 560, 40, 340, sc)
        self.drawLine(40, 340, 80, 340, sc)
        self.drawLine(80, 340, 220, 500, sc)
        self.drawLine(220, 500, 220, 340, sc)
        self.drawLine(220, 340, 260, 340, sc)
        self.drawLine(260, 340, 260, 560, sc)
        self.drawLine(260, 560, 220, 560, sc)
        self.drawLine(220, 560, 80, 400, sc)
        self.drawLine(80, 400, 80, 560, sc)
        self.drawLine(80, 560, 40, 560, sc)

        # Draw the letter 'R'
        self.drawLine(360, 560, 360, 340, sc)
        self.drawLine(360, 340, 510, 340, sc)
        self.drawLine(510, 340, 540, 370, sc)
        self.drawLine(540, 370, 540, 430, sc)
        self.drawLine(540, 430, 510, 460, sc)
        self.drawLine(510, 460, 450, 460, sc)
        self.drawLine(450, 460, 540, 560, sc)
        self.drawLine(540, 560, 490, 560, sc)
        self.drawLine(490, 560, 400, 460, sc)
        self.drawLine(400, 460, 400, 560, sc)
        self.drawLine(400, 560, 360, 560, sc)    
        self.drawLine(400, 430, 400, 370, sc)
        self.drawLine(400, 370, 480, 370, sc)
        self.drawLine(480, 370, 500, 390, sc)
        self.drawLine(500, 390, 500, 410, sc)
        self.drawLine(500, 410, 480, 430, sc)
        self.drawLine(480, 430, 400, 430, sc)