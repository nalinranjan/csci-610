import glfw
import OpenGL.GL.shaders
import math
import numpy as np
from OpenGL.GL import *
from OpenGL.arrays import vbo

def key_callback(window, key, scancode, action, mods):
    if key == glfw.KEY_ESCAPE and action == glfw.PRESS:
        glfw.set_window_should_close(window, 1)

def framebuffer_size_callback(window, width, height):
    glViewport(0, 0, width, height)

def main():
    # Initialize the library
    if not glfw.init():
        return

    glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 4)
    glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 4)
    glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

    width, height = 1280, 720

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(width, height, "GLFW Test", None, None) #glfw.get_primary_monitor()
    if not window:
        glfw.terminate()
        return

    glfw.set_key_callback(window, key_callback)
    glfw.set_framebuffer_size_callback(window, framebuffer_size_callback)

    # Make the window's context current
    glfw.make_context_current(window)
    glfw.swap_interval(1)

    glViewport(0, 0, width, height)
    glClearColor(0.2, 0.2, 0.4, 1.0)

    # atts = GLint(0)
    # glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, atts)
    # print(atts)

    vertices = np.array([-0.5, -0.5, 0.0,
                0.5, -0.5, 0.0,
                0.0, 0.5, 0.0,
                -0.8, 0.8, 0.0,
                -0.4, -0.2, 0.0,
                0.0, 0.8, 0.0], dtype=np.float32)

    vao = glGenVertexArrays(1)
    glBindVertexArray(vao)

    # vertex_buffer = vbo.VBO(vertices, usage='GL_STATIC_DRAW', target='GL_ARRAY_BUFFER')
    # vertex_buffer.bind()
    # vertex_buffer.copy_data()
    vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vbo)
    glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)

    vertex_shader_source = """
    #version 440 core
    layout (location = 0) in vec3 pos;

    void main()
    {
        gl_Position = vec4(pos, 1.0f);
    }
    """

    # vertex_shader = glCreateShader(GL_VERTEX_SHADER)
    # glShaderSource(vertex_shader, vertex_shader_source)
    # glCompileShader(vertex_shader)
    # log = glGetShaderInfoLog(vertex_shader)
    # if log: print(log)

    fragment_shader_source = """
    #version 440 core
    out vec4 color;

    void main()
    {
        color = vec4(1.0f, 0.5f, 0.2f, 1.0f);
    }
    """

    fragment_shader_source2 = """
    #version 440 core
    out vec4 fragColor;
    uniform vec4 color;
    
    void main()
    {
        fragColor = color;
    }
    """

    # fragment_shader = glCreateShader(GL_FRAGMENT_SHADER)
    # glShaderSource(fragment_shader, fragment_shader_source)
    # glCompileShader(fragment_shader)
    # log = glGetShaderInfoLog(fragment_shader)
    # if log: print(log)

    vertex_shader = OpenGL.GL.shaders.compileShader(vertex_shader_source, GL_VERTEX_SHADER)
    fragment_shader = OpenGL.GL.shaders.compileShader(fragment_shader_source, GL_FRAGMENT_SHADER)
    fragment_shader2 = OpenGL.GL.shaders.compileShader(fragment_shader_source2, GL_FRAGMENT_SHADER)

    # shader_program = glCreateProgram()
    # glAttachShader(shader_program, vertex_shader)
    # glAttachShader(shader_program, fragment_shader)
    # glLinkProgram(shader_program)
    # log = glGetProgramInfoLog(shader_program)
    # if log: print(log)

    shader_program = OpenGL.GL.shaders.compileProgram(vertex_shader, fragment_shader)
    shader_program2 = OpenGL.GL.shaders.compileProgram(vertex_shader, fragment_shader2)

    glDeleteShader(vertex_shader)
    glDeleteShader(fragment_shader)
    glDeleteShader(fragment_shader2)

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)
    glEnableVertexAttribArray(0)
    glBindVertexArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    rect_vao = glGenVertexArrays(1)
    glBindVertexArray(rect_vao)
    rect_vertices = np.array([0.8, 0.8, 0.0,
                              0.8, -0.2, 0.0,
                              0.4, -0.2, 0.0,
                              0.4, 0.8, 0.0], dtype=np.float32)
    rect_indices = np.array([0, 1, 2,
                             0, 2, 3], dtype=np.uint32)
    ebo = glGenBuffers(1)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, rect_indices, GL_STATIC_DRAW)
    rect_vbo = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, rect_vbo)
    glBufferData(GL_ARRAY_BUFFER, rect_vertices, GL_STATIC_DRAW)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)
    glEnableVertexAttribArray(0)
    glBindVertexArray(0)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    # glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

    # Loop until the user closes the window
    while not glfw.window_should_close(window):
        # Render here, e.g. using pyOpenGL
        glClear(GL_COLOR_BUFFER_BIT)

        glUseProgram(shader_program)
        glBindVertexArray(rect_vao)
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, None)

        glUseProgram(shader_program2)
        color_location = glGetUniformLocation(shader_program2, "color")
        red = math.sin(glfw.get_time()) / 2 + 0.5
        glUniform4f(color_location, red, 0.0, 0.0, 1.0)
        glBindVertexArray(vao)
        glDrawArrays(GL_TRIANGLES, 0, 6)

        # Swap front and back buffers
        glfw.swap_buffers(window)

        # Poll for and process events
        glfw.poll_events()

    glDeleteBuffers(1, vbo)
    glDeleteBuffers(1, rect_vbo)
    glDeleteVertexArrays(1, vao)
    glDeleteVertexArrays(1, rect_vao)
    glfw.terminate()

if __name__ == "__main__":
    main()
